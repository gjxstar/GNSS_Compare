package com.galfins.gogpsextracts.Glonass;

import android.location.Location;
import android.util.Log;

import com.galfins.gogpsextracts.EphemerisResponse;
import com.galfins.gogpsextracts.Glonass.EphGlonass;
import com.galfins.gogpsextracts.GnssEphemeris;
import com.galfins.gogpsextracts.Iono;
import com.galfins.gogpsextracts.NavigationProducer;
import com.galfins.gogpsextracts.Observations;
import com.galfins.gogpsextracts.SatellitePosition;
import com.galfins.gogpsextracts.Time;

import java.util.ArrayList;

/**
 * <p>
 * Class for parsing RINEX navigation files
 * </p>
 *
 * @author Eugenio Realini, Cryms.com
 */
public class RinexNavigationParserGlonass extends EphemerisSystemGlonass implements NavigationProducer {

    private final String TAG = this.getClass().getSimpleName();

    private Iono iono = null; /* Ionosphere model parameters */

    private ArrayList<EphGlonass> eph = new ArrayList<EphGlonass>(); /* GPS broadcast ephemerides */

    public RinexNavigationParserGlonass(EphemerisResponse ephResponse) {
        for (GnssEphemeris eph : ephResponse.ephList) {
            if (eph instanceof GlonassEphemeris) {
                this.eph.add(new EphGlonass((GlonassEphemeris) eph));
            }
        }
        this.iono = new Iono(ephResponse.ionoProto);
    }

    /**
     * @param unixTime
     * @param satID
     * @return Reference ephemeris set for given time and satellite
     */
    public EphGlonass findEph(long unixTime, int satID, char satType) {

        long dt = 0;
        long dtMin = 0;
        long dtMax = 0;
        long delta = 0;
        EphGlonass refEph = null;

        //long gpsTime = (new Time(unixTime)).getGpsTime();

        for (int i = 0; i < eph.size(); i++) {
            // Find ephemeris sets for given satellite
            if (eph.get(i).getSatID() == satID && eph.get(i).getSatType() == satType) {
                // Consider BeiDou time (BDT) for BeiDou satellites (14 sec difference wrt GPS time)
                if (satType == 'C') {
                    delta = 14000;
                    unixTime = unixTime - delta;
                }
                // Compare current time and ephemeris reference time
                dt = Math.abs(eph.get(i).getRefTime().getMsec() - unixTime /*getGpsTime() - gpsTime*/) / 1000;
                // If it's the first round, set the minimum time difference and
                // select the first ephemeris set candidate; if the current ephemeris set
                // is closer in time than the previous candidate, select new candidate
                if (refEph == null || dt < dtMin) {
                    dtMin = dt;
                    refEph = eph.get(i);
                }
            }
        }

        if (refEph == null)
            return null;
        switch (refEph.getSatType()) {
            case 'R':
                dtMax = 900;
                break;
            case 'J':
                dtMax = 3600;
                break;
            default:
                dtMax = 7200;
                break;
        }
        if (dtMin > dtMax) {
            refEph = null;
        }

        return refEph;
    }

    public boolean isTimestampInEpocsRange(long unixTime) {
        return eph.size() > 0 /*&&
                eph.get(0).getRefTime().getMsec() <= unixTime *//*&&
		unixTime <= eph.get(eph.size()-1).getRefTime().getMsec() missing interval +epochInterval*/;
    }

    public SatellitePosition getSatPositionAndVelocities(long unixTime, double range, int satID, char satType, double receiverClockError) {
        //long unixTime = obs.getRefTime().getMsec();
        //double range = obs.getSatByIDType(satID, satType).getPseudorange(0);
        if (range == 0)
            return null;
        EphGlonass eph = findEph(unixTime, satID, satType);
        if (eph == null) {
            Log.e(TAG, "getSatPositionAndVelocities: Ephemeris failed to load...");
            return null;
        }

        //			char satType = eph.getSatType();

        SatellitePosition sp = computeSatPositionAndVelocities(unixTime, range, satID, satType, eph, receiverClockError);
        //			SatellitePosition sp = computePositionGps(unixTime, satType, satID, eph, range, receiverClockError);
        //if(receiverPosition!=null) earthRotationCorrection(receiverPosition, sp);
        return sp;// new SatellitePosition(eph, unixTime, satID, range);
    }

    @Override
    public Iono getIono(long unixTime) {
        return iono;
    }
}
